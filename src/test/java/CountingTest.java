import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Test;

class CountingTest {

    @Test
    void sum_shouldAddNumbers() {

        //given
        double a1 = 1;
        double a2 = 2;
        double a3 = 3;
        double b1 = -3;
        double b2 = 0;
        double b3 = 4;

        //when
        double expected1 = a1+b1;
        double expected2 = a2+b2;
        double expected3 = a3-b3;

        //then
        assertEquals(expected1, Counting.sum(a1, b1));
        assertEquals(expected2, Counting.sum(a2, b2));
        assertNotEquals(expected3, Counting.sum(a3, b3));
    }

    @Test
    void difference_shouldSubtractNumbers() {

        //given
        double a1 = 1;
        double a2 = 2;
        double a3 = 3;
        double b1 = -3;
        double b2 = 0;
        double b3 = 4;

        //when
        double expected1 = a1-b1;
        double expected2 = a2-b2;
        double expected3 = a3+b3;

        //then
        assertEquals(expected1, Counting.difference(a1, b1));
        assertEquals(expected2, Counting.difference(a2, b2));
        assertNotEquals(expected3, Counting.difference(a3, b3));
    }

    @Test
    void multiply_ShouldMultiplyNumbers() {

        //given
        double a1 = 1;
        double a2 = 2;
        double a3 = 3;
        double b1 = -3;
        double b2 = 0;
        double b3 = 4;

        //when
        double expected1 = a1*b1;
        double expected2 = a2*b2;
        double expected3 = a3-b3;

        //then
        assertEquals(expected1, Counting.multiply(a1, b1));
        assertEquals(expected2, Counting.multiply(a2, b2));
        assertNotEquals(expected3, Counting.multiply(a3, b3));
    }

    @Test
    void divide_shouldDivideNumbers() {

        //given
        double a1 = 1;
        double a2 = 2;
        double a3 = 3;
        double b1 = -3;
        double b2 = 0;
        double b3 = 4;

        //when
        double expected1 = a1/b1;
        double expected2 = a2/b2;
        double expected3 = a3-b3;

        //then
        assertEquals(expected1, Counting.divide(a1, b1));
        assertEquals(expected2, Counting.divide(a2, b2));
        assertNotEquals(expected3, Counting.divide(a3, b3));
    }
}