import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ServletCalculator")
public class ServletCalculator extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String stringA = request.getParameter("a");
        String stringB = request.getParameter("b");
        String stringOperand = request.getParameter("operand");

        double a = 0;
        double b = 0;
        double result = 0;


        try {

            a = Double.parseDouble(stringA);
            b = Double.parseDouble(stringB);

            if (stringOperand.equals("+")) {

                result = Counting.sum(a, b);
                Main.setA(a);
                Main.setB(b);
                Main.setOperand("1");
                Main.setResult(result);
                doSetResult(response, result);
            } else if (stringOperand.equals("-")) {
                result = Counting.difference(a, b);
                Main.setA(a);
                Main.setB(b);
                Main.setOperand("2");
                Main.setResult(result);
                doSetResult(response, result);
            } else if (stringOperand.equals("*")) {
                result = Counting.multiply(a, b);
                Main.setA(a);
                Main.setB(b);
                Main.setOperand("3");
                Main.setResult(result);
                doSetResult(response, result);
            } else if (stringOperand.equals("/") && (b != 0)) {
                result = Counting.divide(a, b);
                Main.setA(a);
                Main.setB(b);
                Main.setOperand("4");
                Main.setResult(result);
                doSetResult(response, result);

            } else {
                Main.setA(0.0);
                Main.setB(0.0);
                Main.setOperand("error");
                Main.setResult(0.0);
                doSetError(response);
            }


        } catch (Exception ex) {
            doSetError(response);
        }


    }

    protected void doSetResult(HttpServletResponse response, double result) throws UnsupportedEncodingException, IOException {

        String reply = "{\"error\":0,\"result\":" + Double.toString(result) + "}";
        response.getOutputStream().write(reply.getBytes("UTF-8"));
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_OK);
    }

    protected void doSetError(HttpServletResponse response) throws UnsupportedEncodingException, IOException {
        String reply = "{\"error\":1}";
        response.getOutputStream().write(reply.getBytes("UTF-8"));
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_BAD_GATEWAY);
    }

}


