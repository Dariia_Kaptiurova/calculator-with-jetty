import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ServletHistory")
public class ServletHistory extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            if (!Main.getOperand().startsWith("er") && !Main.getOperand().startsWith(" ")) {
                System.out.println(Main.getB());
                System.out.println(Main.getA());
                System.out.println(Main.getOperand());
                System.out.println(Main.getResult());
                doSetResult(response , Main.getB(), Main.getA() , Main.getOperand(), Main.getResult());
                System.out.println("sd");
            }

        } catch (Exception ex) {
            doSetError(response);
            System.out.println("error");
        }


    }

    protected void doSetResult(HttpServletResponse response, double b, double a, String operand, double result) throws UnsupportedEncodingException, IOException {

        String reply = "{\"error\":0,\"a\":" + Double.toString(a) + ",\"b\":" + Double.toString(b) + ",\"op\":" + operand + ",\"result\":" + Double.toString(result) + "}";
        response.getOutputStream().write(reply.getBytes("UTF-8"));
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_OK);
    }

    protected void doSetError(HttpServletResponse response) throws UnsupportedEncodingException, IOException {
        System.out.println("ds");
        String reply = "{\"error\":1}";
        response.getOutputStream().write(reply.getBytes("UTF-8"));
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_OK);
    }


}
